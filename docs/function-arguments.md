# Python 함수 인수: 위치 및 키워드

## <a name="intro"></a> 들어가며
이 포스팅에서는 Python에서 함수를 호출할 때 위치 인수와 키워드 인수를 사용하는 방법을 다루게 될 것이다. 또한 위치 인수와 키워드 인수를 혼합하는 방법, 디폴트 인수를 사용하는 방법, 임의 인수를 사용하는 방법, 키워드 전용 인수를 사용하는 방법도 배우게 된다.

함수 인수(function arguments)는 함수를 호출할 때 전달하는 값이다. 이들은 매개변수 또는 입력으로도 알려져 있다. 함수 인수는 함수가 어떻게 작동하고 무엇을 반환하는 지에 영향을 미칠 수 있다. 예를 들어, 두 개의 인수를 취하고 그들의 합을 반환하는 다음 함수를 생각해 보자.

```python
# Define a function that takes two arguments
def add(x, y):
    return x + y

# Call the function with different arguments
print(add(3, 4)) # Prints 7
print(add(10, 20)) # Prints 30
```

이 예에서 함수 `add`는 `x`와 `y`의 두 인수를 취하여 그 합을 반환한다. 함수를 호출하면 `x`와 `y`에 대해 서로 다른 값을 전달할 수 있고 함수는 서로 다른 결과를 반환한다.

Python에서 함수에 인수를 전달하는 방법은 위치 또는 키워드 두 가지가 있다. 위치 인수는 함수의 매개 변수와 같은 순서로 전달된다. 매개 변수의 이름과 등호를 사용하여 키워드 인수를 전달한다. 예를 들어, 두 개의 인수를 가져다가 인사말 메시지를 출력하는 다음 함수를 생각해 보자.

```python
# Define a function that takes two arguments
def greet(name, language):
    if language == "English":
        print("Hello, " + name + "!")
    elif language == "French":
        print("Bonjour, " + name + "!")
    elif language == "Spanish":
        print("Hola, " + name + "!")
    else:
        print("Unknown language")

# Call the function with positional arguments
greet("Alice", "English") # Prints Hello, Alice!
greet("Bob", "French") # Prints Bonjour, Bob!
# Call the function with keyword arguments
greet(language="Spanish", name="Charlie") # Prints Hola, Charlie!
greet(name="David", language="German") # Prints Unknown language
```

이 예에서 함수 `greet`는 `name`과 `language`의 두 인수를 취하고 `language`를 기반으로 인사 메시지를 출력한다. 함수를 호출할 때 인수를 위치 또는 키워드로 전달할 수 있다. 인수를 위치로 전달할 경우 함수의 매개 변수와 동일한 순서를 따라야 한다. 키워드로 인수를 전달할 경우 어떤 순서를 사용해도 되지만 매개 변수의 이름과 등호를 사용하여 지정해야 한다.

위치 및 키워드 인수를 사용하면 코드를 보다 쉽게 읽을 수 있고 유연하게 사용할 수 있다. 그러나 이러한 규칙과 모범 사례를 사용할 때 따라야 할 몇 가지 규칙과 모범 사례가 있다. 다음 섹션에서는 이러한 규칙과 모범 사례 그리고 Python 프로그램에서 효과적으로 사용하는 방법에 대해 자세히 알아본다.

## <a name="sec_02"></a> 함수 인수란?
이 절에서는 함수 인수가 무엇인지, 왜 함수 인수가 Python 프로그래밍에서 유용한지 알수 있을 것이다. 위치 인수와 키워드 인수의 차이점과 이를 올바르게 사용하는 방법도 알게 될 것이다.

함수는 특정한 작업을 수행하는 코드의 블록이다. 함수를 정의하기 위해 `def` 키워드를 사용하고 함수의 이름과 괄호(`()`)를 사용한다. 괄호 안에는 함수가 입력으로 받아들일 수 있는 변수의 이름인 함수의 파라메터를 지정할 수 있다. 예를 들어 `a`와 `b` 두 파라메터를 취하고 그 합을 반환하는 함수를 생각해 보자.

```python
# Define a function that takes two parameters
def add(a, b):
    return a + b
```

함수 인수는 함수를 호출할 때 전달하는 값이다. 함수의 이름과 괄호 한 쌍을 사용하여 함수에 인수를 전달할 수 있다. 괄호 안에는 쉼표로 구분하여 인수의 값을 지정할 수 있다. 예를 들어 함수 `add`에 `3`과 `4`의 두 인수를 전달하는 다음 함수 호출을 생각해 보자.

```python
# Call the function with two arguments
result = add(3, 4)
print(result) # Prints 7
```

함수 인수는 함수가 어떻게 행동하고 무엇을 반환하는 지에 영향을 미칠 수 있다. 함수에 서로 다른 인수를 전달함으로써 함수가 서로 다른 작업을 수행하게 하거나 서로 다른 출력을 내도록 할 수 있다. 예를 들어 함수에 서로 다른 인수를 전달하는 다음과 같은 `add` 함수 호출을 생각해 보자.

```python
# Call the function with different arguments
print(add(10, 20)) # Prints 30
print(add(5, -2)) # Prints 3
print(add("Hello", "World")) # Prints HelloWorld
```

보다시피 함수 `add`는 숫자 및 문자열과 같은 다양한 타입의 인수를 처리하고 인수 값에 따라 다른 결과를 반환할 수 있다.

Python에서 함수에 인수를 전달하는 방법은 위치 또는 키워드 두 가지가 있다. 위치 인수는 함수의 매개 변수와 같은 순서로 전달된다. 매개 변수의 이름과 등호를 사용하여 키워드 인수를 전달한다. 예를 들어 `name`과 `language`의 두 매개 변수를 사용하여 인사 메시지를 출력하는 다음 함수를 생각해 보자.

```python
# Define a function that takes two parameters
def greet(name, language):
    if language == "English":
        print("Hello, " + name + "!")
    elif language == "French":
        print("Bonjour, " + name + "!")
    elif language == "Spanish":
        print("Hola, " + name + "!")
    else:
        print("Unknown language")

# Call the function with positional arguments
greet("Alice", "English") # Prints Hello, Alice!
# Call the function with keyword arguments
greet(language="Spanish", name="Charlie") # Prints Hola, Charlie!
```

이 예에서 함수 `greet`는 `name`과 `language`의 두 파라메터를 취하고 언어를 기반으로 인사 메시지를 출력한다. 함수를 호출할 때 인수를 위치 또는 키워드로 전달할 수 있다. 인수를 위치로 전달할 경우 함수의 파라메터와 동일한 순서를 따라야 한다. 인수를 키워드로 전달할 경우 어떤 순서를 사용해도 되지만 파라메터의 이름과 등호를 지정해야 한다.

위치 및 키워드 인수를 사용하면 코드를 보다 쉽게 읽을 수 있고 유연하게 사용할 수 있다. 그러나 이러한 규칙과 모범 사례를 사용할 때 따라야 할 몇 가지 규칙과 모범 사례가 있습니다. [다음 섹션](#sec_03)에서는 이러한 규칙과 모범 사례 그리고 Python 프로그램에서 효과적으로 사용하는 방법에 대해 자세히 알아본다.

## <a name="sec_03"></a> 위치 인수 사용법
이 절에서는 Python에서 함수를 호출할 때 위치 인수를 사용하는 방법을 다룰 것이다. 위치 인수를 사용하는 규칙과 모범 사례, 일반적인 오류와 함정을 피하는 방법도 설명한다.

위치 인수는 Python에서 함수에 인수를 전달하는 가장 일반적이고 간단한 방법이다. 위치 인수는 함수의 매개 변수와 같은 순서로 전달된다. 예를 들어, `a`와 `b`의 두 매개 변수를 취하고 그 합을 반환하는 다음 함수를 생각해 보자.

```python
# Define a function that takes two parameters
def add(a, b):
    return a + b


# Call the function with positional arguments
result = add(3, 4)
print(result) # Prints 7
```

이 예에서 함수 `add`은 `a`와 `b`의 두 매개변수를 취하여 그 합을 반환한다. 함수를 호출하면 함수의 매개변수와 같은 순서로 `3`과 `4`라는 두 위치 인수를 전달한다. 함수는 첫 번째 인수를 첫 번째 매개변수에 할당하고 두 번째 인수를 두 번째 매개변수에 할당한다. 그런 다음 두 매개변수의 합을 반환한다.

위치 인수를 사용할 때는 다음 규칙을 따라야 한다.

- 함수의 매개변수의 갯수와 같은 갯수의 인수로 호출하여야 한다. 매개변수보다 인수를 더 많거나 적으면 오류가 발생한다. 예를 들어, 두 개의 매개변수만 취하는 함수 `add`에 세 개의 인수로 다음 함수 호출한다고 생각해 보자.

```python
# Call the function with three arguments
result = add(3, 4, 5)
print(result) # Raises an error
```

이 함수 호출은 함수 `add`가 두 개의 위치 인수를 취하지만 세 개가 주어졌다는 `TypeError`를 발생시킨다.

- 함수의 매개변수와 같은 순서로 인수를 전달해야 한다. 당신이 인수를 다른 순서로 전달하면 당신은 잘못된 결과를 얻게 될 것이다. 예를 들어, 인수를 역순으로 전달한 다음과 같은 함수 호출을 함수 `add`에 대해 생각해 보자.

```python
# Call the function with arguments in reverse order
result = add(4, 3)
print(result) # Prints 7
```

이 함수 호출은 오류를 발생시키지는 않는다. 인수들이 달라도 이전 함수 호출과 동일한 결과를 반환할 것이다. 함수 `add`는 인수들의 이름은 신경 쓰지 않고 위치만 신경 쓰기 때문이다. 따라서 첫 번째 인수는 첫 번째 매개변수에, 두 번째 인수는 두 번째 매개변수에 값에 관계없이 할당된다.

위치 인수를 사용할 때는 다음 모범 사례를 따르는 것을 권장한다.

- 함수의 매개변수를 설명적이고 의미 있는 이름을 사용하여 함수를 더 읽기 쉽고 이해하기 쉽게 만들어야 한다. 예를 들어 `a`와 `b`를 함수 `add`의 매개변수로 사용하는 대신 `num1`과 `num2`를 사용하면 함수가 두 개의 숫자를 입력으로 예상함을 나타낼 수 있다.

```python
# Define a function that takes two parameters with descriptive names
def add(num1, num2):
    return num1 + num2

# Call the function with positional arguments
result = add(3, 4)
print(result) # Prints 7
```

- 주석과 docstring을 사용하여 함수의 목적과 행동, 파라메터의 의미와 타입, 반환값을 문서화해야 한다. 예를 들어 함수 정의 위에 주석을 추가하고 함수 본문 안에 docstring을 추가하여 함수 `add`의 기능과 사용 방법을 설명할 수 있다.

```python
# A function that takes two numbers as inputs and returns their sum
def add(num1, num2):
    """Returns the sum of num1 and num2.
    Parameters:
    num1 (int or float): The first number to add
    num2 (int or float): The second number to add
    Returns:
    int or float: The sum of num1 and num2
    """
    return num1 + num2

# Call the function with positional arguments
result = add(3, 4)
print(result) # Prints 7
```

위치 인수는 Python에서 함수에 인수를 전달하는 간단하고 일반적인 방법이다. 그러나 함수에 많은 매개 변수가 있거나 매개 변수의 순서가 직관적이지 않을 때는 제한적이고 오류가 발생하기 쉬울 수 있다. [다음 절](#sec_04)에서는 위치 인수의 한계와 단점을 일부 극복할 수 있는 키워드 인수를 사용하는 방법을 알아 볼 것이다.

## <a name="sec_04"></a> 키워드 인수 사용법
이 절에서는 Python에서 함수를 호출할 때 키워드 인수를 사용하는 방법을 설명할 것이다. 키워드 인수를 사용할 때의 장단점, 위치 인수와 결합하는 방법도 일아 볼 것이다.

Python에서 함수에 인수를 전달하는 또 다른 방법으로 키워드 인수가 있다. 키워드 인수는 매개변수의 이름과 등호를 사용하고 인수의 값을 사용하여 전달된다. 예를 들어, `name`과 `language`의 두 매개변수를 사용하여 인사말 메시지를 출력하는 다음 함수를 생각해 보자.

```python
# Define a function that takes two parameters
def greet(name, language):
    if language == "English":
        print("Hello, " + name + "!")
    elif language == "French":
        print("Bonjour, " + name + "!")
    elif language == "Spanish":
        print("Hola, " + name + "!")
    else:
        print("Unknown language")

# Call the function with keyword arguments
greet(language="Spanish", name="Charlie") # Prints Hola, Charlie!
```

이 예에서 함수 `greet`는 `name`과 `language`의 두 매개변수를 취하고 `language`를 기반으로 인사말 메시지를 출력한다. 함수를 호출하면 매개변수의 이름과 등호를 사용하여 `language="Spanish"`와 `name="Charlie"`의 두 가지 키워드 인수를 전달한다. 함수는 각 인수의 값을 순서에 관계없이 해당 매개변수에 할당한다. 그런 다음 적절한 인사말 메시지를 출력한다.

키워드 인수를 사용하면 다음과 같은 이점을 누릴 수 있다.

매개변수의 정확한 이름만 사용하면 어떤 순서로든 인수를 전달할 수 있다. 이것은 특히 함수에 매개변수가 많거나 매개변수의 순서가 직관적이지 않을 때 코드를 더 유연하고 읽기 쉽게 만들 수 있다. 예를 들어 `name`, `age`, `gender`와 `occupation` 네 매개변수를 사용하여 간단한 소개를 인쇄하는 다음 함수를 생각해 보자.

```python
# Define a function that takes four parameters
def introduce(name, age, gender, occupation):
    print("My name is " + name + ".")
    print("I am " + str(age) + " years old.")
    print("I am a " + gender + ".")
    print("I work as a " + occupation + ".")

# Call the function with keyword arguments
introduce(occupation="teacher", gender="female", name="Emma", age=25)
```

이 예에서 함수 도입부는 `name`, `age`, `gender`와 `occupation` 네 매개변수를 취하여 간략한 소개문을 출력한다. 함수를 호출하면 함수의 매개변수와 다른 순서로 `occupation="teacher"`, `gender="female"`, `name="Emma"`와 `age=25` 등 네 키워드 인수를 전달한다. 함수는 순서에 관계없이 각 인수의 값을 해당 매개변수에 할당한다. 그런 다음 매개변수와 동일한 순서로 소개문을 출력한다.

- 함수가 매개변수에 대한 디폴트 값을 가지고 있다면 함수를 호출할 때 일부 인수를 생략할 수 있다. 이것은 특히 함수가 많은 매개변수를 가지고 있거나 디폴트 값이 일반적일 때 코드를 더 간결하게 만들고 불필요한 반복을 피할 수 있다. 예를 들어 `name`, `language`와 `greeting`의 세 매개변수를 사용하고 커스터마이즈된 인사말 메시지를 출력하는 다음 함수를 생각해 보자. 함수는 매개변수 인사말에 대한 기본값을 가지고 있으며 이는 "Hello"이다.

```python
# Define a function that takes three parameters, one with a default value
def greet(name, language, greeting="Hello"):
    if language == "English":
        print(greeting + ", " + name + "!")
    elif language == "French":
        print(greeting + ", " + name + "!")
    elif language == "Spanish":
        print(greeting + ", " + name + "!")
    else:
        print("Unknown language")

# Call the function with keyword arguments, omitting the greeting argument
greet(name="Alice", language="English") # Prints Hello, Alice!
greet(language="French", name="Bob") # Prints Hello, Bob!
```

이 예에서 함수 `greet`는 `name`, `language`와 `greeting` 세 파라미터를 취하고 커스터마이즈된 그리팅 메시지를 인쇄한다. 함수는 파라미터 `greeting`에 대한 디폴트 값을 가지며, 이는 `"Hello"`이다. 함수를 호출할 때 `name`과 `language`의 두 키워드 인수를 전달하고 `greeting` 인수를 생략한다. 함수는 각 인수의 값을 해당 파라미터에 할당하고 누락된 파라메터에 대한 디폴트 값을 사용한다. 그런 다음 디폴트 greeting으로 greeting 메시지를 출력한다.

키워드 인수를 사용할 때는 다음과 같은 단점을 인식해야 한다.

- 매개변수의 정확한 이름을 사용해야 하며 그렇지 않으면 오류가 발생한다. 함수의 매개변수 중 어느 것과도 일치하지 않는 이름을 사용하면 함수가 예기치 않은 키워드 인수를 얻었다는 `TypeError`가 나타난다. 예를 들어 잘못된 이름의 인수를 함수 `greet`에 전달하는 다음 함수 호출을 생각해 보자.

```python
# Call the function with a wrong argument name
greet(name="Charlie", lang="Spanish") # Raises an error
```

이 함수 호출은 함수 `greet`이 예기치 않은 키워드 인수 `lang`을 얻었다는 `TypeError`를 발생시킨다. 함수는 `lang`이 아닌 `language`라는 이름의 파라메터를 기대하기 때문이다.

위치 인수와 키워드 인수 모두에 동일한 이름을 사용하는 것을 피해야 합니다. 그렇지 않으면 오류가 발생한다. 위치 인수와 키워드 인수 모두에 동일한 이름을 사용하면 함수가 동일한 인수에 대해 여러 값을 얻었다는 `SyntaxError`가 나타난다. 예를 들어 위치 인수와 키워드 인수 모두에 대해 동일한 이름을 함수 `greet`에 전달하는 다음 함수 호출을 생각해 보자.

```python
# Call the function with the same name for both a positional and a keyword argument
greet("Alice", name="Charlie") # Raises an error
```

이 함수 호출은 함수 `greet`가 인수 `name`에 대한 여러 값을 얻었다는 `SyntaxError`를 발생시킬 것이다. 이는 함수가 첫 번째 위치 인수를 첫 번째 매개 변수인 이름에 할당한 다음 키워드 인수 `name="Charlie"`를 동일한 매개 변수에 할당하려고 하기 때문인데, 이는 허용되지 않는다.

키워드 인수를 사용할 때 다음 규칙을 따르는 한 위치 인수와 결합할 수도 있다.

키워드 인수 앞에 위치 인수를 전달해야 하며, 그렇지 않으면 함수가 키워드 인수 뒤에 위치 인수가 있다는 `SyntaxError`을 얻을 것이다. 예를 들어, 키워드 인수 뒤에 위치 인수를 함수 `greet`에 전달하는 다음 함수 호출을 생각해 보자.

```python
# Call the function with a positional argument after a keyword argument
greet(language="Spanish", "Charlie") # Raises an error
```

이 함수 호출은 함수` greet`가 키워드 인수 뒤에 위치 인수가 있다는 `SyntaxError`를 발생시킬 것이다. 함수는 위치 인수가 키워드 인수 뒤가 아니라 앞에 나오기를 기대하기 때문이다.

같은 인수를 위치 인수로 한 번, 키워드 인수로 한 번씩 두 번 전달하는 것을 피해야 한다. 예를 들어, 같은 인수를 위치 인수로 한 번, 키워드 인수로 한 번씩 두 번 전달하는 다음 함수 호출을 함수 `greet`로 생각해 보자.

```python
# Call the function with the same argument twice, once as a positional argument and once as a keyword argument
greet("Alice", name="Charlie") # Raises an error
```

이 함수 호출은 함수 `greet`가 인수 `name`에 대한 여러 값을 얻었다는 `SyntaxError`를 발생시킬 것이다. 이는 함수가 첫 번째 위치 인수를 첫 번째 매개 변수에 할당하기 때문이다.

## <a name="sec_05"></a> 위치 및 키워드 인수를 혼합하는 방법
이 절에서는 Python에서 함수를 호출할 때 위치 인수와 키워드 인수를 혼합하는 방법을 배울 것이다. 위치 인수와 키워드 인수를 혼합하는 것의 장점과 단점, 그리고 공통적인 오류와 함정을 피하는 방법도 설명할 것이다.

위치 인수와 키워드 인수를 혼합하는 것은 Python에서 위치 인수와 키워드 인수의 장점을 모두 결합한 함수 인수를 전달하는 방법이다. 위치 인수와 키워드 인수를 혼합하면 어떤 규칙을 따르는 한 어떤 인수는 위치별로, 어떤 인수는 키워드별로 전달할 수 있다. 예를 들어 `name`, `language`와 `greeting`이라는 세 매개 변수를 가지고 커스터마이즈된 인사말 메시지를 출력하는 다음 함수를 생각해 보자. 이 함수는 매개 변수 `greeting`에 대한 디폴트 값을 가지고 있으며 이것는 `Hello`이다.

```python
# Define a function that takes three parameters, one with a default value
def greet(name, language, greeting="Hello"):
    if language == "English":
        print(greeting + ", " + name + "!")
    elif language == "French":
        print(greeting + ", " + name + "!")
    elif language == "Spanish":
        print(greeting + ", " + name + "!")
    else:
        print("Unknown language")

# Call the function with a mix of positional and keyword arguments
greet("Alice", greeting="Hi", language="English") # Prints Hi, Alice!
```

이 예에서 함수 `greet`는 `name`, `language`와 `greeting`의 세 파라메터를 취하고 커스터마이즈된 그리팅 메시지를 인쇄한다. 함수는 파라메터 `greeting`에 대한 디폴트 값을 가지며, 이 값은 `Hello`이다. 함수를 호출하면 위치와 키워드 인수인 `"Alice"`, `greeting="Hi"`와 `language="English"`의 혼합을 전달한다. 함수는 첫 번째 위치 인수를 첫 번째 파라메터에 할당하고 키워드 인수를 대응하는 파라메터에 순서에 관계없이 할당한다. 그런 다음 커스터마이즈된 `greeting`으로 그리팅 메시지를 인쇄한다.

위치 인수와 키워드 인수를 혼합하면 다음과 같은 이점을 누릴 수 있다.

- 키워드 인수들에 대한 파라메터들의 정확한 이름들을 사용하는 한, 임의의 순서로 인수들을 패스할 수 있다. 이것은, 특히 함수가 많은 파라메터들을 가지거나 파라메터들의 순서가 직관적이지 않을 때, 코드를 더 유연하고 판독가능하게 만들 수 있다. 예를 들어, 이전의 예에서, 키워드 인수들에 대한 파라메터들의 이름들을 사용하는 한, 당신은 임의의 순서로 인수들을 패스할 수 있다.

```python
# Call the function with a different order of arguments
greet(greeting="Hi", language="English", name="Alice") # Prints Hi, Alice!
greet(language="English", name="Alice", greeting="Hi") # Prints Hi, Alice!
greet("Alice", language="English", greeting="Hi") # Prints Hi, Alice!
```

- 함수가 파라미터에 대한 디폴트 값을 갖는 경우, 일부 인수를 생략할 수 있다. 이는 특히 함수가 많은 파라미터를 갖거나 디폴트 값이 일반적인 경우 코드를 더 간결하게 만들고 불필요한 반복을 방지할 수 있다. 예를 들어, 앞의 예에서 `greeting` 인수를 생략할 수 있으며, 함수는 `"Hello"`를 디폴트 값으로 사용할 것이다.

```python
# Call the function without the greeting argument
greet("Alice", language="English") # Prints Hello, Alice!
```

위치 인수와 키워드 인수를 혼합할 때 다음과 같은 단점을 인식해야 한다.

- 키워드 인수 앞에 위치 인수를 전달해야 한다. 그렇지 않으면 함수가 키워드 인수 뒤에 위치 인수를 있다는` SyntaxError`가 발생한다. 예를 들어 이전 예제에서는 `name` 인수가 위치 인수이고 `language` 인수가 키워드이기 때문에 `name` 인수 앞에 `language` 인수를 전달할 수 없다.


```python
# Call the function with a positional argument after a keyword argument
greet(language="English", "Alice", greeting="Hi") # Raises an error
```

- 같은 인수를 위치 인수로 한 번, 키워드 인수로 한 번 두 번 전달하는 것을 피해야 하며, 그렇지 않으면 함수가 같은 인수에 대해 여러 값을 얻었다는 `SyntaxError`가 나타날 것이다. 예를 들어 앞의 예제에서 이름 인수를 위치 인수로 한 번, 키워드 인수로 한 번 두 번 전달할 수 없는데, 이는 함수가 각 매개변수에 대해 하나의 값만 기대하기 때문이다.

```python
# Call the function with the same argument twice, once as a positional argument and once as a keyword argument
greet("Alice", name="Charlie", language="English") # Raises an error
```

위치 인수와 키워드 인수를 혼합하는 것은 Python에서 함수에 인수를 전달하는 강력하고 유연한 방법이다. 그러나 함수에 많은 매개 변수가 있거나 매개 변수가 디폴트 값을 가질 때 혼란스럽고 오류가 발생하기 쉬울 수도 있다. [다음 절](#sec_06)에서는 함수 정의와 함수 호출을 단순화할 수 있는 디폴트 인수를 사용하는 방법을 샆펴볼 것이다.

## <a name="sec_06"></a> 디폴트 인수 사용법
이 절에서는 Python에서 함수를 정의하고 호출할 때 디폴트 인수를 사용하는 방법을 설명할 것이다. 디폴트 인수를 사용할 때의 장단점, 일반적인 오류와 함정을 피하는 방법도 알려줄 것이다.

디폴트 인수는 함수의 일부 매개 변수에 대한 디폴트 값을 지정하여 함수를 호출할 때 이러한 인수를 생략할 수 있도록 하는 방법이다. 디폴트 인수는 함수 정의와 함수 호출을 단순화하고 코드를 보다 간결하고 유연하게 만들 수 있다. 예를 들어 `name`, `language`와 `greeting`의 세 매개 변수를 사용하고 커스터마이즈된 인사 메시지를 출력하는 다음 함수를 생각해 보자. 함수는 매개 변수 `greeting`에 대한 디폴트 값을 가지고 있으며 이 값은 `"Hello"`이다.

```python
# Define a function that takes three parameters, one with a default value
def greet(name, language, greeting="Hello"):
    if language == "English":
        print(greeting + ", " + name + "!")
    elif language == "French":
        print(greeting + ", " + name + "!")
    elif language == "Spanish":
        print(greeting + ", " + name + "!")
    else:
        print("Unknown language")

# Call the function without the greeting argument
greet("Alice", language="English") # Prints Hello, Alice!
```

이 예에서 함수 `greet`는 `name`, `language`와 `greeting`의 세 파라메터를 취하고 커스터마이즈된 그리팅 메시지를 출력한다. 함수는 파라메터 `greeting`에 대한 디폴트 값을 가지며, 이 값은 `"Hello"`이다. 함수를 정의할 때 파라메터 이름 뒤에 등호를 사용하고 그 다음에 값을 선언하여 파라메터에 대한 디폴트 값을 지정할 수 있다. 함수를 호출할 때 `greeting` 인수를 생략할 수 있으며, 이경우 함수는 기본값인 `"Hello"`를 사용할 것이다.

디폴트 인수를 사용하면 다음과 같은 이점을 누릴 수 있다.

- 사용자가 일부 작동을 일정하게 유지하면서 함수 동작의 일부 작동을 커스터마이징할 수 있게 함으로써 함수를 보다 유연하고 적응 가능하게 만들 수 있다. 예를 들어, 이전 예에서, 함수 `greet`는 사용자가 다른 값을 지정하지 않는 한, `greeting`을 일정하게 유지하면서 사용자가 그리팅 메시지의 `name`과 `language`를 커스터마이징할 수 있게 한다.

```python
# Call the function with different name and language arguments
greet("Bob", language="French") # Prints Hello, Bob!
greet("Charlie", language="Spanish") # Prints Hello, Charlie!

# Call the function with a different greeting argument
greet("David", language="English", greeting="Hi") # Prints Hi, David!
```

- 불필요한 반복과 장황함을 피함으로써 함수를 더 간결하고 우아하게 만들 수 있다. 예를 들어, 앞의 예에서 함수 `greet`는 디폴트 인수를 사용함으로써 다른 언어에 대해 동일한 인사말을 반복하는 것을 피한다. 이것은 함수 정의와 함수 호출을 더 짧고 깨끗하게 만든다.

디폴트 인수를 사용할 때는 다음과 같은 단점을 인식해야 한다.

- 디폴트 값이 아닌 인수 뒤에 디폴트 인수를 정의해야 하며, 그렇지 않으면 함수가 디폴트 인수 뒤에 디폴트 값이 아닌 인수가 있다는 `SyntaxError`가 발생한다는 것이다. 예를 들어 이전 예에서는 디폴트 값이 아닌 인수 `language` 앞에 디폴트 인수 `greeting`으로 함수 `greet`를 정의할 수 없다.

```python
# Define a function with a default argument before a non-default argument
def greet(name, greeting="Hello", language): # Raises an error
    # Function body
```

- 변경 가능한 객체를 디폴트 인수로 사용하는 것은 피해야 하며, 그렇지 않으면 예상치 못한 결과를 얻을 수 있다. 변경 가능한 객체는 리스트, 사전, 집합 등과 같이 생성된 후에 변경할 수 있는 객체를 말한다. 변경 가능한 객체를 디폴트 인수로 사용하면 함수는 모든 함수 호출에서 동일한 객체를 사용하게 되며, 객체가 변경되면 이후 함수 호출에 영향을 미친다. 예를 들어, 디폴트 값으로 빈 리스트를 갖는 매개 변수 항목을 가져다가 리스트에 숫자를 추가하여 인쇄하는 다음 함수를 생각해 보자.

```python
# Define a function with a mutable object as a default argument
def append_number(items=[]):
    items.append(len(items) + 1)
    print(items)

# Call the function without the items argument
append_number() # Prints [1]
append_number() # Prints [1, 2]
append_number() # Prints [1, 2, 3]
```

이 예에서 함수 `append_number`는 디폴트 값으로 빈 리스트를 갖는 매개변수 항목을 가져와 리스트에 숫자를 추가하여 출력한다. 함수를 정의하면 항목의 디폴트 값은 단 한 번만 생성되고 함수는 모든 함수 호출에 대해 동일한 리스트 객체를 사용한다. 함수를 호출하면 함수는 리스트 객체에 숫자를 부가하여 수정하고 수정된 목록을 출력한다. 그러나 수정은 다음 함수 호출에 대해 지속되고 함수는 동일한 리스트에 다른 숫자를 부가하여 더 긴 목록을 출력한다. 이러한 동작은 특히 함수가 모든 함수 호출에 대해 새로운 목록을 사용하기를 기대한다면 혼란스럽고 바람직하지 않을 수 있다.

디폴트 인수를 사용할 때 몇 가지 규칙을 따르는 한 위치 인수와 키워드 인수를 혼합할 수도 있다. [다음 절](#sec_07)에서는 임의 인수를 함수에 전달할 수 있는 임의 인수를 사용하는 방법을 다룰 것이다.

## <a name="sec_07"></a> 임의 인수 사용법
이 절에서는 Python에서 함수를 정의하고 호출할 때 임의 인수를 사용하는 방법을 설명할 것이다. 또한 임의 인수를 사용할 때의 장단점과 이를 효율적으로 처리하는 방법도 배울 것이다.

임의 인수는 매개 변수의 수나 이름을 지정하지 않고 Python에서 함수에 임의의 갯수의 인수를 허용하는 방법이다. 임의 인수는 사용자가 원하는 만큼 인수를 전달하거나 적게 전달함으로써 함수를 보다 유연하고 적응력 있게 만들 수 있다. 예를 들어 임의 갯수의 인수를 사용하여 모든 인수를 출력하는 다음 함수를 생각해 보자.

```python
# Define a function that takes an arbitrary number of arguments
def print_args(*args):
    for arg in args:
        print(arg)

# Call the function with different numbers of arguments
print_args(1, 2, 3) # Prints 1, 2, 3
print_args("Hello", "World") # Prints Hello, World
print_args() # Prints nothing
```

이 예에서 함수 `print_args`는 임의 갯수의 인수를 가져와 모두 인쇄한다. 함수를 정의할 때 매개 변수 이름 앞에 별표(`*`)를 사용하여 매개 변수가 임의 갯수의 인수를 수락할 수 있음을 나타낼 수 있다. 함수는 매개 변수를 불변(immutable) 시퀀스인 튜플로 취급한다. 함수를 호출할 때 쉼표로 구분하여 임의 갯수의 인수를 전달할 수 있으며 함수는 튜플을 반복하여 각 인수를 인쇄한다.

임의 인수를 사용하면 다음과 같은 이점을 갖을 수 있다.

- 사용자가 사전에 지정할 필요 없이 임의 갯수의 인수를 함수에 전달할 수 있도록 함으로써 함수를 보다 유연하고 적응력 있게 만들 수 있다. 예를 들어, 앞의 예에서 `print_args` 함수는 `0`부터 무한대까지 임의 갯수의 인수를 처리하고 모두 인쇄할 수 있다.

```python
# Call the function with more arguments
print_args(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) # Prints 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
```

- 불필요한 반복과 장황함을 피함으로써 함수를 더 간결하고 우아하게 만들 수 있다. 예를 들어 앞의 예제에서 `print_args` 함수는 임의 갯수의 인수를 수용할 수 있는 단일 매개 변수를 사용함으로써 여러 매개 변수와 인수를 정의할 필요가 없다.

임의의 인수를 사용할 경우 다음과 같은 단점을 인식해야 한다.

- 매개변수 이름 앞에 별표(`*`)를 사용해야 하며, 그렇지 않으면 오류가 발생한다. 별표를 생략하면 함수는 매개변수를 정규 매개변수로 처리하고 인수는 하나만 기대한다. 예를 들어 앞의 예제에서 `print_args` 함수를 별표 없이 정의하면 함수는 하나의 위치 인수를 취하지만 더 많은 인수가 주어졌다고 `TypeError`를 발생시킨다.

```python
# Define a function without the asterisk
def print_args(args): # Raises an error
    for arg in args:
        print(arg)

# Call the function with more than one argument
print_args(1, 2, 3) # Raises an error
```

- 임의 인수를 튜플로 처리해야 한다. 이는 함수 내부에서 인수에 값을 추가, 제거, 변경 등 수정할 수 없음을 의미한다. 인수를 수정하려면 튜플을 리스트와 같이 변경 가능한 객체로 변환한 다음 리스트를 수정해야 한다. 예를 들어 임의 갯수의 인수를 취하고 인수 리스트를 역순으로 반환하는 함수를 생각해 보자.

```python
def reverse_args(*args):
    # Convert the tuple to a list
    args_list = list(args)
    # Reverse the list
    args_list.reverse()
    # Return the list
    return args_list

# Call the function with different numbers of arguments
print(reverse_args(1, 2, 3)) # Prints [3, 2, 1]
print(reverse_args("Hello", "World")) # Prints ['World', 'Hello']
print(reverse_args()) # Prints []
```

이 예에서 함수 `reverse_args`는 임의 갯수의 인수를 취하여 인수 목록을 역순으로 반환한다. 함수는 리스트 함수를 이용하여 인수 튜플을 리스트로 변환한 다음, 역순으로 목록을 반환한다. 그러면 함수는 리스트를 반환한다.

임의 인수를 사용할 때 몇 가지 규칙을 따르는 한 위치 인수, 키워드 인수와 디폴트 인수와 혼합할 수도 있다. [다음 절](#sec_08)에서는 매개 변수의 이름을 사용하지 않고 키워드만으로 인수를 전달할 수 있는 키워드 전용 인수를 사용하는 방법을 설명할 것이다.

## <a name="sec_08"></a> 키워드 전용 인수 사용법
이 절에서는 Python에서 함수를 정의하고 호출할 때 키워드 전용 인수를 사용하는 방법을 배울 것이다. 키워드 전용 인수를 사용할 때의 장단점, 일반적인 오류와 함정을 피하는 방법도 다룰 것이다.

키워드 전용 인수는 함수의 일부 매개 변수를 위치가 아닌 키워드로만 전달할 수 있도록 지정하는 방법이다. 키워드 전용 인수는 사용자가 인수를 전달할 때 매개 변수의 이름을 사용하도록 강제함으로써 함수를 보다 명확하고 명시적으로 만들 수 있다. 예를 들어 `name`, `language`와 `greeting` 세 매개 변수를 사용하고 커스터마이즈된 인사 메시지를 출력하는 함수를 생각해 보자. 함수에는 매개 변수 `greeting`에 대한 디폴트 값인 `"Hello"`가 있다. 함수에는 인사 메시지의 대문자화 여부를 결정하는 키워드 전용 매개 변수도 있다. 함수에는 매개 변수의 `capitalized` 디폴트 값인 `"False"`가 있다.

```python
# Define a function that takes three parameters, one with a default value and one keyword-only
def greet(name, language, greeting="Hello", *, capitalized=False):
    if language == "English":
        message = greeting + ", " + name + "!"
    elif language == "French":
        message = greeting + ", " + name + "!"
    elif language == "Spanish":
        message = greeting + ", " + name + "!"
    else:
        message = "Unknown language"
    # Check if the message should be capitalized
    if capitalized:
        message = message.upper()
    print(message)

# Call the function with a mix of positional and keyword arguments, including the keyword-only argument
greet("Alice", "English", capitalized=True) # Prints HELLO, ALICE!
```

이 예에서 함수 `greet`는 `name`, `language`와 `greeeting`의 세 매개변수를 사용하여 맞춤형 인사말 메시지를 출력한다. 함수는 매개변수 인사말에 대한 디폴트 값인 `"Hello"`를 갖는다. 함수는 인사말 메시지를 대문자로 써야 하는지 아닌지를 결정하는 키워드 전용 매개변수도 갖는다. 함수는 파라메터를 대문자로 쓰는 디폴트 값인 `"False"`를 갖는다. 함수를 정의할 때 정규 매개변수 뒤에 별표(`*`)를 사용하여 다음 매개변수가 키워드 전용임을 나타낼 수 있다. 함수는 키워드 전용 매개변수를 사전으로 취급하며, 이는 키와 값의 불변 매핑이다. 함수를 호출할 때 매개변수의 이름과 등호를 사용하여 키워드 전용 인수를 전달해야 하며, 그렇지 않으면 오류가 발생한다. 함수는 각 인수의 값을 해당 매개변수에 할당하고 맞춤형 인사말 메시지를 출력한다.

키워드 전용 인수를 사용하면 다음과 같은 이점을 얻을 수 있다.

- 사용자가 인수를 전달할 때 매개변수의 이름을 사용하도록 강제함으로써 함수를 더 명확하고 명시적으로 만들 수 있다. 이것은 특히 함수가 많은 매개변수를 가지거나 매개변수의 의미가 명확하지 않을 때 코드를 더 읽기 쉽고 이해하기 쉽게 만들 수 있다. 예를 들어, 이전 예에서 함수 `greet`는 사용자가 인수를 전달할 때 `capitalized` 매개변수의 이름을 사용하도록 강제하며, 이는 함수 호출을 더 명확하고 명시적으로 만든다.

```python
# Call the function with the keyword-only argument
greet("Alice", "English", capitalized=True) # Prints HELLO, ALICE!
```

- 일반 파라미터의 순서나 수에 영향을 주지 않고, 사용자가 함수 동작의 일부 측면을 사용자에 맞출 수 있게 함으로써, 함수를 보다 유연하고 적응 가능하게 만들 수 있다. 예를 들어, 이전 예에서, 함수 `greet`는 `name`, `language`와 `greeeting` 파라미터의 순서나 수를 변경하지 않고, 인사 메시지가 인쇄되는 방식에 영향을 미치는 `capitalized` 파라메터를 맞춤화할 수 있게 한다.

```python
# Call the function with different values for the keyword-only argument
greet("Bob", "French", greeting="Bonjour") # Prints Bonjour, Bob!
greet("Charlie", "Spanish", greeting="Hola", capitalized=True) # Prints HOLA, CHARLIE!
```

키워드 전용 인수를 사용할 때는 다음과 같은 단점에 유의해야 한다.

- 일반 매개변수 뒤에 별표(`*`)를 사용해야 하며, 그렇지 않으면 오류가 발생한다. 별표를 생략하면 함수는 키워드 전용 매개변수를 정규 매개변수로 처리하고, 위치 또는 키워드로 전달될 것으로 예상한다. 예를 들어 앞의 예에서 만약 함수 `greet`를 별표 없이 정의한다면 함수는 일반 매개변수 4개로 예상하고, 함수가 2개의 위치 인수를 취하지만 1개만 주어졌다고 `TypeError`를 발생시킨다.

<!-- 위의 마지막 문장은 원본에 오류가 있어 수정 하였음.  -->

```python
# Define a function without the asterisk
def greet(name, language, greeting="Hello", capitalized=False): # Raises an error
    # Function body

# Call the function with three arguments
greet("Alice", greeting="Hello", capitalized=True) # Raises an error
```

```
TypeError: greet() missing 1 required positional argument: 'language'
```

- 일반 매개변수와 키워드 전용 매개변수 모두에 대해 동일한 이름을 사용하는 것을 피해야 한다. 그렇지 않으면 함수가 동일한 이름을 가진 여러 매개변수를 가지고 있다는 `SyntaxError`가 나타날 것이다. 예를 들어, 이전 예에서 일반 매개변수와 키워드 전용 매개변수 모두에 대해 동일한 이름을 가진 함수 `greet`를 정의할 수 없는데, 이는 함수가 각각 이름에 대하여 매개변수를 하나만 기대하기 때문이다.

```python
# Define a function with the same name for both a normal and a keyword-only parameter
def greet(name, language, greeting="Hello", *, name): # Raises an error
    # Function body
```

키워드 전용 인수를 사용할 때 몇 가지 규칙을 따른다면 위치, 키워드, 기본 및 임의 인수와 혼합할 수도 있다. [다음 절](#conclusion)에서는 이 포스팅을 요약하고 마무리 한다.

## <a name="summary"></a> 요약 및 결론
이 포스팅에서는 Python에서 함수를 정의하고 호출할 때 위치 인수와 키워드 인수를 사용하는 방법을 배웠다. 위치 인수와 키워드 인수를 혼합하는 방법, 기본 인수를 사용하는 방법, 임의 인수를 사용하는 방법, 키워드 전용 인수를 사용하는 방법도 다루었다.

다음은 기억해야 할 몇 가지 핵심 사항이다.

- 위치 인수들은 함수의 매개 변수들과 같은 순서로 전달된다. 그들은 단순하고 일반적이지만, 특히 함수가 많은 매개 변수를 가지거나 매개 변수의 순서가 직관적이지 않을 때, 제한적이고 오류가 발생하기 쉽다.
- 매개변수의 이름과 등호를 사용하여 키워드 인수를 전달한다. 이들은 더 유연하고 읽기 쉽지만, 특히 함수에 매개변수가 거의 없거나 매개변수의 의미가 명백할 때 장황하고 중복될 수 있다.
- 키워드 인수 앞에서 위치 인수를 전달한다면 위치 인수와 키워드 인수를 혼합할 수 있다. 이는 위치 인수와 키워드 인수의 장점을 모두 결합할 수 있지만, 특히 함수에 많은 매개 변수가 있거나 매개 변수가 기본값을 가질 때 혼동되고 오류가 발생하기 쉬울 수 있다.
- 디폴트 인수를 사용하여 함수의 일부 매개 변수에 대한 디폴트 값을 지정할 수 있으므로 함수를 호출할 때 이러한 인수를 생략할 수 있다. 이렇게 하면 함수 정의와 함수 호출을 단순화할 수 있지만 특히 변경 가능한 개체를 기본 인수로 사용할 때 예상치 못한 결과를 초래할 수도 있다.
- 매개변수의 갯수나 이름을 지정하지 않고 임의의 인수를 사용하여 함수에 임의 갯수의 인수를 허용할 수 있다. 이는 함수를 보다 유연하고 적응 가능하게 만들 수 있지만, 특히 인수를 튜플로 처리해야 할 때 함수를 덜 명확하고 명시적으로 만들 수도 있다.
- 키워드 전용 인수를 사용하여 함수의 일부 파라미터는 위치가 아닌 키워드로만 전달될 수 있음을 지정할 수 있다. 이는 함수를 더 명확하고 명시적으로 만들 수 있지만, 특히 함수가 파라메터 갯수가 적거나 파라메터의 이름이 긴 경우 함수를 덜 간결하고 덜 우아하게 만들 수도 있다.

이러한 다양한 타입의 인수을 사용하면 함수를 더 강력하고 다양하게 사용할 수 있으며 코드를 더 쉽게 읽고 이해할 수 있도록 만들 수 있다. 그러나 이러한 인수를 사용할 때는 몇 가지 규칙과 모범 사례를 따라야 하며 일반적인 오류와 함정을 피해야 한다.
